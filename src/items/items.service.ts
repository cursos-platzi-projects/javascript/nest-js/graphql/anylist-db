import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateItemInput, UpdateItemInput } from './dto/inputs';
import { Item } from './entities/item.entity';

@Injectable()
export class ItemsService {

  constructor(
    /** Se injecta el objeto para poder usar el 'Repository' del objeto 'Item' */
    @InjectRepository( Item )
    private readonly itemsRepository: Repository<Item>,

  ) {}


  /** Se usa 'async' para usar el 'await' ya que es una espera de la base de datos */ 
  /** Return: Se retorna una promesa por lo de asyn-await y el objeto que se quiere regresar */
  async create( createItemInput: CreateItemInput ): Promise<Item> {
    /** Prepara la query para salvar los datos */
    const newItem = this.itemsRepository.create( createItemInput )
    /** newItem.name = "another name"; */
    /** Antes de darle save, se pueden modificar los datos */
    /** Despues se usa el metodo 'save' para guardar los datos en la base de datos */
    return await this.itemsRepository.save( newItem );
  }

  async findAll(): Promise<Item[]> {
    // TODO: filtrar, paginar, por usuario...
    return this.itemsRepository.find();
  }

  async findOne( id: string ): Promise<Item> {
    const item = await this.itemsRepository.findOneBy({ id })

    if ( !item ) throw new NotFoundException(`Item with id: ${ id } not found`);

    return item;
  }

  async update(id: string, updateItemInput: UpdateItemInput): Promise<Item> {
    
    const item = await this.itemsRepository.preload( updateItemInput );

    if ( !item ) throw new NotFoundException(`Item with id: ${ id } not found`);

    return this.itemsRepository.save( item );

  }

  async remove( id: string ):Promise<Item> {
    // TODO: soft delete, integridad referencial
    const item = await this.findOne( id );
    await this.itemsRepository.remove( item );
    return { ...item, id };
  }
}
